import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogradouroComponent } from './logradouro.component';

const routes: Routes = [

    {
        path: '',
        component: LogradouroComponent
    }
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class LogradouroRoutingModule { }
