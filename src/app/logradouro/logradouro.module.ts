import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogradouroComponent } from './logradouro.component'
import { LogradouroRoutingModule } from './logradouro.routing.module';


@NgModule({
  declarations: [LogradouroComponent],
  imports: [
    CommonModule,
    LogradouroRoutingModule
  ]
})
export class LogradouroModule { }
